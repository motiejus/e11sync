_: prev: {
  python3 = prev.python3.override {
    packageOverrides = _: python-prev: {
      # when used from a different flake (motiejus/config) on x86_64-linux
      # (vno1-oh2) when cross-compiling to aarch64-linux build (fra1-a),
      # it would not use cache. I wasn't sure at the time if the package
      # was cached altogether. But disabling tests made it build reasonably
      # quickly when cross-compiling too.
      django = python-prev.django_5.overridePythonAttrs {doCheck = false;};
    };
  };
}
