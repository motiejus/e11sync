geoip2-tarball: let
  django5 = import ./django5.nix;
in
  _: prev: {
    geoip-mmdb = (prev.extend django5).callPackage ../pkgs/geoip-mmdb.nix {
      inherit geoip2-tarball;
    };
    e11sync-blog = (prev.extend django5).callPackage ../pkgs/e11sync-blog.nix {};
    e11sync-static = (prev.extend django5).callPackage ../pkgs/e11sync-static.nix {};
    e11sync-djangostatic = (prev.extend django5).callPackage ../pkgs/e11sync-djangostatic.nix {};
    e11sync-caddyfile = (prev.extend django5).callPackage ../pkgs/e11sync-caddyfile.nix {};
    e11sync-backend = (prev.extend django5).callPackage ../pkgs/e11sync-backend.nix {};
  }
