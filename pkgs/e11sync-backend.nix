{
  lib,
  stdenvNoCC,
  runCommand,
  makeWrapper,
  python3,
  libmaxminddb,
  uwsgi,
  backendPort ? 8002,
  databasePath ? null,
  geoip-mmdb,
  e11sync-djangostatic,
}: let
  uwsgi-python = uwsgi.override {plugins = ["python3"];};
  pythonEnv = python3.withPackages (ps: [ps.django ps.geoip2]);
in
  stdenvNoCC.mkDerivation {
    name = "e11sync-backend";
    propagatedBuildInputs = [
      pythonEnv
      libmaxminddb
    ];
    nativeBuildInputs = [makeWrapper];
    src = ../app;
    patchPhase = ''patchShebangs --build manage.py'';
    buildPhase = ''mkdir -p $out'';
    installPhase = ''
      cp -r . $out/app
      makeWrapper $out/app/manage.py $out/bin/e11sync \
        ${lib.optionalString (databasePath != null) ''
        --set E11SYNC_DATABASE_PATH "${databasePath}" \
      ''} \
        --set GEOIP_PATH "${geoip-mmdb}"

      makeWrapper ${uwsgi-python}/bin/uwsgi $out/bin/e11sync-backend \
        --chdir $out/app \
        --set-default E11SYNC_HTTP_PORT ${toString backendPort} \
        --add-flags "--need-plugin ${uwsgi-python}/lib/uwsgi/python3_plugin.so" \
        --add-flags "--python-path ${pythonEnv}/lib/${pythonEnv.libPrefix}/site-packages" \
        --add-flags "--http-socket 127.0.0.1:${toString backendPort}" \
        --add-flags "--wsgi-file e11sync/wsgi.py" \
        --add-flags "--master --need-app" \
        ${lib.optionalString (databasePath != null) ''
        --set E11SYNC_DATABASE_PATH "${databasePath}" \
      ''} \
        --set E11SYNC_STATIC_ROOT "${e11sync-djangostatic.passthru.manifest}" \
        --set E11SYNC_DEBUG "" \
        --set GEOIP_PATH "${geoip-mmdb}" \
    '';
    passthru.tests.unit =
      runCommand "e11sync-test" {
        srcs = ../app;
        buildInputs = [pythonEnv];
      } ''
        unpackPhase
        patchShebangs --build app/manage.py
        export GEOIP_PATH="${geoip-mmdb}"
        export E11SYNC_STATIC_ROOT="${e11sync-djangostatic.passthru.manifest}"
        app/manage.py test app
        mkdir -p $out
      '';
  }
