{
  runCommand,
  zopfli,
  brotli,
  findutils,
  e11sync-blog,
  e11sync-djangostatic,
}:
runCommand "e11sync-static" {
  srcs = [e11sync-djangostatic e11sync-blog];
  nativeBuildInputs = [findutils];
  sourceRoot = ".";
} ''
  unpackPhase

  if ! cmp \
      e11sync-djangostatic/style.*.css \
      e11sync-blog/static/style.*.css
    then
      echo "style.*.css in blog and djangostatic do not match:"
      ls -l e11sync-djangostatic/style/style.*.css blog/style/style.*.css
      exit 1
  fi

  rm e11sync-djangostatic/style.*.css

  mkdir -p $out
  mv e11sync-blog/{static,blog,contact} $out/
  mv --no-clobber e11sync-djangostatic/* $out/static/

  find $out \
      -name '*.css' -or \
      -name '*.js' -or \
      -name '*.svg' -or \
      -name '*.html' -or \
      -name '*.txt' | \
      tee >(xargs -n1 -P''$(nproc) ${zopfli}/bin/zopfli) | \
      xargs -n1 -P''$(nproc) ${brotli}/bin/brotli
''
