{
  stdenvNoCC,
  dart-sass,
  hugo,
}:
stdenvNoCC.mkDerivation rec {
  name = "e11sync-blog";
  srcs = [../blog ../static];
  nativeBuildInputs = [dart-sass];
  sourceRoot = ".";
  buildPhase = ''
    cd blog
    ${hugo}/bin/hugo --printPathWarnings --panicOnWarning
  '';
  installPhase = ''
    mkdir -p $out
    mv public/{static,blog,contact} $out/
  '';
}
