{
  writeTextFile,
  e11sync-static,
  backendPort ? 8002,
}:
writeTextFile {
  name = "e11sync-caddyfile";
  text = ''
    @addSlash path /static /blog /contact
    route @addSlash {
      redir {uri}/ 302
    }

    header /static/* Cache-Control "public, max-age=31536000, immutable"

    header {
      Content-Security-Policy "default-src 'none'; style-src 'self' 'unsafe-inline'; img-src 'self' data:; script-src 'self'; frame-ancestors 'none'"
      Cross-Origin-Opener-Policy same-origin
      Referrer-Policy same-origin
      X-Content-Type-Options nosniff

      -X-Frame-Options
      -Last-Modified
    }

    @staticRoutes path /static/* /contact/* /blog/*
    route @staticRoutes {
      file_server * {
        root ${e11sync-static}
        precompressed br gzip
      }
    }

    route {
      encode gzip
      reverse_proxy http://127.0.0.1:${toString backendPort}
    }
  '';
}
