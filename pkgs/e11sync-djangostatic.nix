{
  stdenvNoCC,
  runCommand,
  python3Packages,
  findutils,
  dart-sass,
  jq,
}: let
  self = stdenvNoCC.mkDerivation {
    name = "e11sync-djangostatic";
    srcs = [../app ../static];
    sourceRoot = ".";
    nativeBuildInputs = [
      python3Packages.django
      dart-sass
      findutils
    ];
    patchPhase = ''patchShebangs --build app/manage.py'';
    buildPhase = ''
      make -C static style.css
      app/manage.py collectstatic

      # remove unversioned files
      pushd app/_static
      ${jq}/bin/jq -r '.paths | keys[]' staticfiles.json | xargs rm
      popd
    '';
    installPhase = ''find; mv app/_static $out'';

    passthru.manifest = runCommand "e11sync-djangostatic-manifest" {} ''
      mkdir -p $out
      cp ${self}/staticfiles.json $out/
    '';
  };
in
  self
