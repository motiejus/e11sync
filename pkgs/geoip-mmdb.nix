{
  stdenvNoCC,
  geoip2-tarball,
}:
stdenvNoCC.mkDerivation {
  name = "geoip-mmdb";
  srcs = [geoip2-tarball];
  installPhase = ''
    mkdir -p $out
    cp GeoLite2-ASN.mmdb GeoLite2-City.mmdb GeoLite2-Country.mmdb $out/
  '';
}
