{
  lib,
  pkgs,
  ...
}: {
  e11sync = {
    enable = true;
    migrateOnStart = true;
    secretKeyPath = "unsafe";
    vhost = ":8001";
  };

  services.caddy = {
    enable = true;
    globalConfig = ''
      http_port 8001
      auto_https off
      debug
    '';
    virtualHosts.":8001".extraConfig = lib.mkForce ''
      redir /admin/* http://{host}:8003{uri}
      ${builtins.readFile "${pkgs.e11sync-caddyfile}"}
    '';
    virtualHosts."http://:8003".extraConfig = lib.mkForce ''
      redir / /admin/
      ${builtins.readFile "${pkgs.e11sync-caddyfile}"}
    '';
  };

  environment.systemPackages = with pkgs; [
    tmux
    htop
  ];

  nix.extraOptions = ''experimental-features = nix-command flakes'';
  users = {
    mutableUsers = false;
    users.nixos = {
      extraGroups = ["wheel"];
      isNormalUser = true;
      hashedPassword = "";
    };
  };
  virtualisation = {
    graphics = false;
    forwardPorts = [
      {
        from = "host";
        host.port = 8001;
        guest.port = 8001;
      }
      {
        from = "host";
        host.port = 8003;
        guest.port = 8003;
      }
    ];
  };
  security.sudo.wheelNeedsPassword = false;
  services.getty.autologinUser = "nixos";
  networking = {
    hostName = "vm";
    firewall.allowedTCPPorts = [8001 8003];
  };
  system.stateVersion = "23.11";
}
