{
  nixConfig = {
    trusted-substituters = "https://cache.nixos.org/";
    trusted-public-keys = "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=";
    extra-experimental-features = "nix-command flakes";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
    geoip2-tarball = {
      url = "https://dl.jakstys.lt/_/2024.01.13.tar.zst";
      flake = false;
    };

    flake-compat.url = "github:nix-community/flake-compat";
    gitignore.url = "github:hercules-ci/gitignore.nix";
    gitignore.inputs.nixpkgs.follows = "nixpkgs";

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
        flake-compat.follows = "flake-compat";
        flake-utils.follows = "flake-utils";
        gitignore.follows = "gitignore";
      };
    };
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    pre-commit-hooks,
    geoip2-tarball,
    ...
  }: let
    overlay = import ./overlays/default.nix geoip2-tarball;
  in
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [overlay (import ./overlays/django5.nix)];
      };
    in {
      packages = {
        inherit
          (pkgs)
          e11sync-blog
          e11sync-static
          e11sync-djangostatic
          e11sync-caddyfile
          e11sync-backend
          ;
      };

      checks = {
        e11sync-unit = pkgs.e11sync-backend.passthru.tests.unit;
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            deadnix.enable = true;
            statix.enable = true;
            flake8.enable = true;
            autopep8 = {
              enable = true;
              entry = "${pkgs.python3Packages.autopep8}/bin/autopep8 -i";
              files = "\\.py$";
            };
            markdownlint.enable = true;
            unit-tests = {
              enable = false;
              # ?? this is a directory, how do I run the script?
              #entry = pkgs.e11sync-backend.passthru.tests.unit.buildCommand;
              pass_filenames = false;
            };
          };
        };
        build-vm = self.nixosConfigurations.vm.config.system.build.vm;
      };

      apps = {
        e11sync-caddy = {
          type = "app";
          name = "e11sync-caddyfile";
          program = toString (pkgs.writeShellScript "wrapper" ''
            exec ${pkgs.caddy}/bin/caddy run --config ${pkgs.writeTextFile {
              name = "Caddyfile";
              text = ''
                {
                  http_port 8001
                  auto_https off
                  debug
                }
                :8001
                ${builtins.readFile "${pkgs.e11sync-caddyfile}"}
              '';
            }} --adapter caddyfile'');
        };
        e11sync-backend = {
          type = "app";
          name = "e11sync-backend";
          program = toString (pkgs.writeShellScript "wrapper" ''
            export E11SYNC_DATABASE_PATH=$PWD/db.sqlite3
            exec ${pkgs.e11sync-backend}/bin/e11sync-backend
          '');
        };
      };

      devShells.default = pkgs.mkShellNoCC {
        packages = [
          (
            pkgs.python3.withPackages
            (p: with p; [django geoip2 django-debug-toolbar flake8 autopep8])
          )
          pkgs.libmaxminddb
          pkgs.dart-sass

          pkgs.hugo
        ];
        GEOIP_PATH = "${pkgs.geoip-mmdb}";
        E11SYNC_DEBUG = "1";
        E11SYNC_STATICFILES = "simple";
        LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
        inherit (self.checks.${system}.pre-commit-check) shellHook;
      };

      formatter = pkgs.alejandra;
    })
    // {
      nixosConfigurations.vm = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {nixpkgs.overlays = [overlay];}
          "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
          ./vm.nix
          self.nixosModules.e11sync
        ];
      };
      nixosModules.e11sync = import ./modules/e11sync geoip2-tarball;

      overlays.default = overlay;
    };
}
