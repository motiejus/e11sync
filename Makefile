# unit tests independent of your environment. Aspires to be the default CI job.
.PHONY: check
test:
	nix flake check

.PHONY: format
format:
	autopep8 -a -a --recursive --in-place --exclude app/signup/migrations app

.PHONY: run
run:
	app/manage.py runserver

.PHONY: run-backend
run-backend:
	nix run .#e11sync-backend

.PHONY: run-frontend
run-frontend:
	nix run .#e11sync-caddyfile

.PHONY: vm
vm:
	nix run .#nixosConfigurations.vm.config.system.build.vm
