---
title: "Contact"
description: "11sync.net contact details"
---

- **Email**: [hello@11sync.net](mailto:hello@11sync.net)
- **Libera.chat**: [#11sync on Libera Chat](https://web.libera.chat/#11sync)

Unless otherwise stated, &copy;Motiejus Jakštys. [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/).
