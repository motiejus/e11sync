from pathlib import Path
from os import environ

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

_DEBUG = bool(environ.get('E11SYNC_DEBUG', False))
_STATIC_ROOT = environ.get('E11SYNC_STATIC_ROOT', BASE_DIR / '_static')
_GEOIP_PATH = environ.get('GEOIP_PATH')

_STATICFILES_STORAGE = environ.get('E11SYNC_STATICFILES', 'manifest')
if _STATICFILES_STORAGE == 'manifest':
    _prefix = 'lib.staticfiles.'
    _STATICFILES_STORAGE_BACKEND = _prefix + 'ManifestStaticFilesStorageSha256'
elif _STATICFILES_STORAGE == 'simple':
    _prefix = 'django.contrib.staticfiles.storage.'
    _STATICFILES_STORAGE_BACKEND = _prefix + 'StaticFilesStorage'
else:
    raise Exception("E11SYNC_STATICFILES neither 'manifest' nor 'simple'")

if db := environ.get('E11SYNC_DATABASE_PATH'):
    _DATABASE_PATH = db
else:
    _DATABASE_PATH = BASE_DIR / 'db.sqlite3'

if secret_path := environ.get("E11SYNC_SECRET_KEY_PATH"):
    with open(secret_path) as f:
        _SECRET_KEY = f.read().strip()
        secret_key_msg = "SECRET_KEY read from {}".format(secret_path)
else:
    _SECRET_KEY = 'django-insecure-$e2!=indeed'
    secret_key_msg = "SECRET_KEY is insecure"

# otherwise it gets printed twice
if not environ.get("E11SYNC_DEBUG_PRINTED"):
    print("DEBUG={}".format(_DEBUG))
    print(secret_key_msg)
    print("STATIC_ROOT={}".format(_STATIC_ROOT))
    print("GEOIP_PATH={}".format(_GEOIP_PATH))
    print("DATABASE_PATH={}".format(_DATABASE_PATH))
    print("STATICFILES_STORAGE_BACKEND=" + _STATICFILES_STORAGE_BACKEND)
    environ["E11SYNC_DEBUG_PRINTED"] = "1"

#######################################
# No more side effects after this place

SECRET_KEY = _SECRET_KEY

DEBUG = _DEBUG

ALLOWED_HOSTS = ["127.0.0.1", "localhost", "11sync.net", "admin.11sync.net"]

INTERNAL_IPS = ["127.0.0.1"]

# Application definition

INSTALLED_APPS = (['debug_toolbar'] if DEBUG else []) + [
    'e11sync.apps.E11SyncConfig',
    'signup.apps.SignupConfig',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = (
    ['debug_toolbar.middleware.DebugToolbarMiddleware'] if DEBUG else []) + [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'e11sync.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'e11sync.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': _DATABASE_PATH,
    }
}

_password_validators = [
    'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    'django.contrib.auth.password_validation.MinimumLengthValidator',
    'django.contrib.auth.password_validation.CommonPasswordValidator',
    'django.contrib.auth.password_validation.NumericPasswordValidator',
]
AUTH_PASSWORD_VALIDATORS = [{'NAME': v} for v in _password_validators]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

STATIC_URL = 'static/'

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STORAGES = {"staticfiles": {"BACKEND": _STATICFILES_STORAGE_BACKEND}}

STATIC_ROOT = _STATIC_ROOT

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": "WARNING",
    },
}

GEOIP_PATH = _GEOIP_PATH
