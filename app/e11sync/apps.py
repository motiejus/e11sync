from django.apps import AppConfig


class E11SyncConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'e11sync'
