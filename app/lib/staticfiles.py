from hashlib import sha256

from django.contrib.staticfiles.storage import ManifestStaticFilesStorage


class ManifestStaticFilesStorageSha256(ManifestStaticFilesStorage):
    def file_hash(self, name, content=None):
        """
        Return a hash of the file with the given name and optional content.
        """
        if content is None:
            return None
        hasher = sha256(usedforsecurity=False)
        for chunk in content.chunks():
            hasher.update(chunk)
        return hasher.hexdigest()
