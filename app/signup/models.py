from django.db import models


class Signup(models.Model):
    email = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True)
    anonymized_ip = models.GenericIPAddressField()
    user_agent = models.CharField(max_length=255, blank=True)
    geoip = models.JSONField(null=True, blank=True)
    referrer = models.CharField(max_length=255, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name="unique_email", fields=["email"]),
        ]

    def __str__(self):
        return self.email

    def place_name(self):
        if geoip := self.geoip:
            return "{}, {}".format(geoip["city"], geoip["country_name"])
        return "??"
