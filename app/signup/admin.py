from django.contrib import admin

from .models import Signup


@admin.register(Signup)
class SignupAdmin(admin.ModelAdmin):
    _all_fields = [
        "email",
        "created_at",
        "anonymized_ip",
        "user_agent",
        "geoip",
        "referrer"
    ]

    date_hierarchy = "created_at"
    list_display = ["email", "created_at", "user_agent"] + ["place_name"]
    list_filter = ["email", "user_agent"]
    ordering = ["created_at"]
    readonly_fields = _all_fields + ["place_name"]
