from django.test import TestCase, Client
from django.urls import reverse

from .models import Signup


class SignupViewTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index(self):
        resp = self.client.get(reverse("index"))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Hosted Syncthing")

    def test_signup_ok(self):
        resp = self.client.post(reverse("index"), {"email": "foo@example.com"},
                                follow=True,
                                HTTP_USER_AGENT="foo-agent",
                                REMOTE_ADDR="127.0.0.2",
                                HTTP_REFERRER="example.org/good-place")
        self.assertEqual(resp.redirect_chain, [('/?success=subscribed', 302)])
        self.assertContains(resp, "You are now subscribed!")
        obj = Signup.objects.get()
        self.assertEqual(obj.email, "foo@example.com")
        self.assertEqual(obj.anonymized_ip, "127.0.0.0")
        self.assertEqual(obj.user_agent, "foo-agent")
        self.assertEqual(obj.referrer, "example.org/good-place")

    def test_signup_badmail(self):
        resp = self.client.post(reverse("index"), {"email": "x@x.x"})
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Enter a valid email address.")
