geoip2-tarball: {
  config,
  lib,
  pkgs,
  ...
}: let
  overlay = import ../../overlays/default.nix geoip2-tarball;
  pkgs1 = pkgs.extend overlay;
  inherit (pkgs1) e11sync-backend e11sync-caddyfile;
in {
  options.e11sync = with lib.types; {
    enable = lib.mkEnableOption "Enable e11sync";
    secretKeyPath = lib.mkOption {type = oneOf [path (enum ["unsafe"])];};
    secretKeyUnsafe = lib.mkOption {
      type = bool;
      default = false;
    };
    migrateOnStart = lib.mkOption {
      type = bool;
      default = false;
    };
    backendPort = lib.mkOption {
      type = port;
      default = 8002;
    };
    databasePath = lib.mkOption {
      type = path;
      default = "/var/lib/e11sync-backend/db.sqlite3";
    };
    vhost = lib.mkOption {type = str;};
  };

  config = let
    cfg = config.e11sync;
  in
    lib.mkIf cfg.enable {
      environment.systemPackages = [
        e11sync-backend
      ];

      systemd.services = {
        e11sync-backend = {
          description = "e11sync backend";
          environment = lib.mkMerge [
            {
              TZ = "UTC";
              E11SYNC_DATABASE_PATH = cfg.databasePath;
            }
            (lib.mkIf (cfg.secretKeyPath != "unsafe") {
              E11SYNC_SECRET_KEY_PATH = "/run/credentials/e11sync-backend.service/secret_key";
            })
          ];
          wantedBy = ["multi-user.target"];
          serviceConfig = lib.mkMerge [
            {
              Type = "notify";
              NotifyAccess = "all";
              Restart = "on-failure";
              RuntimeDirectory = "e11sync-backend";
              StateDirectory = "e11sync-backend";
              WorkingDirectory = "/var/lib/e11sync-backend";
              KillSignal = "SIGQUIT";
              ExecStart = "${e11sync-backend}/bin/e11sync-backend";

              MemoryHigh = "1535M";
              MemoryMax = "4096M";
              LimitNOFILE = 1048576;
              DynamicUser = true;
              NoNewPrivileges = true;
              PrivateDevices = true;
              ProtectKernelTunables = true;
              ProtectControlGroups = true;
            }
            (lib.mkIf cfg.migrateOnStart {
              ExecStartPre = "${e11sync-backend}/bin/e11sync migrate";
            })
            (lib.mkIf (cfg.secretKeyPath != "unsafe") {
              LoadCredential = "secret_key:${cfg.secretKeyPath}";
            })
          ];
        };
      };

      services.caddy.virtualHosts."${cfg.vhost}".extraConfig =
        lib.mkDefault
        (builtins.readFile "${e11sync-caddyfile}");
    };
}
